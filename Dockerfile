FROM cypress/browsers:node14.16.0-chrome90-ff88

RUN curl -fsSL https://get.docker.com -o get-docker.sh
RUN chmod +x ./get-docker.sh
RUN sh get-docker.sh
RUN rm get-docker.sh

